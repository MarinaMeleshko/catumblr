﻿using System.Linq;
using System.Security.Claims;
using Catumblr.BLL.ServiceInterfaces;
using Catumblr.Services;
using Microsoft.AspNetCore.Mvc;

namespace Catumblr.Components
{
    public class Statistic : ViewComponent
    {
        private readonly IPostService _postService;

        public Statistic(IPostService postService)
        {
            _postService = postService;
        }

        public IViewComponentResult Invoke(int postCount)
        {
            var userId = UserClaimsPrincipal.FindFirstValue(ClaimTypes.NameIdentifier);
            var allPosts = _postService.GetUserPosts(userId).ToList();
            var popularPosts = _postService.GetMostPopularPosts(userId, postCount);
            var likeCount = allPosts.Sum(post => post.Likes.Count);
            var commentCount = allPosts.Sum(post => post.Comments.Count);
            var statisticViewModel = ViewModelService.GetStatisticViewModel(popularPosts, likeCount, commentCount);
            return View("_Statistic", statisticViewModel);
        }
    }
}
