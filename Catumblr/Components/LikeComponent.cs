﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catumblr.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Catumblr.Components
{
    public class Like : ViewComponent
    {
        public IViewComponentResult Invoke(PostViewModel model)
        {
            return View(model.IsLiked ? "_Liked" : "_Unliked", model);
        }
    }
}
