﻿"use strict";

function createCommentBody(commentText, userName) {
    const element = document.createElement("div");
    element.classList.add("media");

    const time = document.createElement("small");
    time.classList.add("pull-right");
    time.innerHTML = dateFormat(new Date(), "dd.mm.yy HH:MM:ss");
    const timeWrapper = document.createElement("h5");
    timeWrapper.appendChild(time);
    element.appendChild(timeWrapper);

    const body = document.createElement("div");
    body.classList.add("media-body");
    const name = document.createElement("h5");
    name.classList.add("media-heading");
    name.classList.add("user_name");
    name.innerHTML = userName;
    body.appendChild(name);

    const text = document.createElement("text");
    text.innerHTML = commentText;
    body.appendChild(text);

    element.appendChild(body);

    return element;
}

commentHubConnection.on("Add",
    function(postId, commentText, userName) {
        if (postId !== parseInt($("#PostId").val())) return;

        const commentCountElement = document.getElementById("commentCount");
        const count = parseInt(commentCountElement.innerHTML) + 1;
        commentCountElement.innerHTML = count + " comments";

        const element = createCommentBody(commentText, userName);

        document.getElementById("commentList").insertBefore(element, document.getElementById("firstComment"));
    });

const addCommentButton = document.getElementById("addCommentButton");
addCommentButton.addEventListener("click",
    function (event) {
        commentHubConnection.invoke("Add", parseInt($("#PostId").val()), $("#Text").val())
            .catch(function(err) {
                return console.error(err.toString());
            });
        event.preventDefault();
    });