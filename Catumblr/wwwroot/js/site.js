﻿const likeHubConnection = new signalR.HubConnectionBuilder().withUrl("/like")
    .configureLogging(signalR.LogLevel.Information).build();

const commentHubConnection = new signalR.HubConnectionBuilder().withUrl("/comment")
    .configureLogging(signalR.LogLevel.Information).build();

likeHubConnection.start().catch(function(err) {
    return console.error(err.toString());
});
commentHubConnection.start().catch(function(err) {
    return console.error(err.toString());
});