﻿"use strict";

const heart = document.getElementById("likeButton");
let isLiked = $("#IsLiked").val() === "True" ? true : false;

if (isLiked) {
    heart.classList.add("text-danger");
} else {
    heart.classList.add("text-info");
}

likeHubConnection.on("Like",
    function (postId, isPostLiked) {
        if (postId !== parseInt($("#PostId").val())) return;
        const countElement = document.getElementById("likeCount");
        const count = parseInt(countElement.innerHTML);

        if (isPostLiked) {
            countElement.innerHTML = count - 1;
            heart.classList.remove("text-danger");
            heart.classList.add("text-info");
        } else {
            countElement.innerHTML = count + 1;
            heart.classList.remove("text-info");
            heart.classList.add("text-danger");
        }
    });

heart.addEventListener("click",
    function (event) {
        const id = parseInt($("#PostId").val());
        likeHubConnection.invoke("Like", id, isLiked).catch(function(err) {
            return console.error(err.toString());
        });
        isLiked = !isLiked;
        event.preventDefault();
    });