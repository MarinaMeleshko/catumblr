﻿"use strict";

likeHubConnection.on("Like",
    function (postId, isPostLiked) {
        const likeCountStatistic = document.getElementById("likeCountStatistic");
        let commonLikeCount = parseInt(likeCountStatistic.innerHTML);
        isPostLiked ? commonLikeCount-- : commonLikeCount++;
        likeCountStatistic.innerHTML = commonLikeCount + " likes,";
    });

commentHubConnection.on("Add",
    function(comment) {
        const commentCountStatistic = document.getElementById("commentCountStatistic");
        const commonCommentCount = parseInt(commentCountStatistic.innerHTML) + 1;
        commentCountStatistic.innerHTML = commonCommentCount + " comments  ";
    });