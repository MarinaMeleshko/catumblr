﻿using System.Collections.Generic;
using Catumblr.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Catumblr.TagHelpers
{
    public class PageLinkTagHelper : TagHelper
    {
        private readonly IUrlHelperFactory _urlHelperFactory;
        public PageLinkTagHelper(IUrlHelperFactory helperFactory)
        {
            _urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        [HtmlAttributeName(DictionaryAttributePrefix = "page-url-")]
        public Dictionary<string, object> PageUrlValues { get; set; } = new Dictionary<string, object>();
        public string PageAction { get; set; }

        public PaginationViewModel Pagination { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (Pagination.TotalPageCount < 2) return;
            var urlHelper = _urlHelperFactory.GetUrlHelper(ViewContext);
            output.TagName = "div";

            var tag = new TagBuilder("ul");
            tag.AddCssClass("pagination");

            CreateFirstPageLink(tag, urlHelper);
            CreateCurrentPreviousNextPagesLinks(tag, urlHelper);
            CreateLastPageLink(tag, urlHelper);

            output.Content.AppendHtml(tag);
        }

        private void CreateFirstPageLink(TagBuilder tag, IUrlHelper urlHelper)
        {
            if (Pagination.PageNumber == 1 || Pagination.PageNumber == 2) return;
            CreateItem(1, tag, urlHelper);
            CreateDecorativeItem("<<", tag);
        }

        private void CreateLastPageLink(TagBuilder tag, IUrlHelper urlHelper)
        {
            if (Pagination.PageNumber == Pagination.TotalPageCount ||
                Pagination.PageNumber == Pagination.TotalPageCount - 1) return;
            CreateDecorativeItem(">>", tag);
            CreateItem(Pagination.TotalPageCount, tag, urlHelper);
        }

        private void CreateCurrentPreviousNextPagesLinks(TagBuilder tag, IUrlHelper urlHelper)
        {
            if (Pagination.HasPreviousPage)
            {
                CreateItem(Pagination.PageNumber - 1, tag, urlHelper);
            }

            CreateItem(Pagination.PageNumber, tag, urlHelper);

            if (Pagination.HasNextPage)
            {
                CreateItem(Pagination.PageNumber + 1, tag, urlHelper);
            }
        }

        private void CreateItem(int pageNumber, TagBuilder tag, IUrlHelper urlHelper)
        {
            var item = CreateTag(pageNumber, urlHelper);
            tag.InnerHtml.AppendHtml(item);
        }

        private static void CreateDecorativeItem(string value, TagBuilder tag)
        {
            var decorativeItem = CreateTag(value);
            tag.InnerHtml.AppendHtml(decorativeItem);
        }

        private TagBuilder CreateTag(int pageNumber, IUrlHelper urlHelper)
        {
            var item = new TagBuilder("li");
            var link = new TagBuilder("a");
            if (pageNumber == Pagination.PageNumber)
            {
                item.AddCssClass("active");
            }
            else
            {
                PageUrlValues["skipCount"] = Pagination.PageSize * (pageNumber - 1);
                PageUrlValues["takeCount"] = Pagination.PageSize;
                link.Attributes["href"] = urlHelper.Action(PageAction, PageUrlValues);
            }
            link.InnerHtml.Append(pageNumber.ToString());
            item.InnerHtml.AppendHtml(link);
            return item;
        }

        private static TagBuilder CreateTag(string value)
        {
            var item = new TagBuilder("li");
            var link = new TagBuilder("a");
            link.InnerHtml.Append(value);
            item.InnerHtml.AppendHtml(link);
            return item;
        }
    }
}
