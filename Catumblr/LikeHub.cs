﻿using System;
using System.Threading.Tasks;
using Catumblr.BLL.ServiceInterfaces;
using Microsoft.AspNetCore.SignalR;

namespace Catumblr
{
    public class LikeHub : Hub
    {
        private readonly ILikeService _likeService;

        public LikeHub(ILikeService likeService)
        {
            _likeService = likeService;
        }
        public async Task Like(int id, bool isLiked)
        {
            if (!Context.User.Identity.IsAuthenticated)
            {
                return;
            }
            if (isLiked)
            {
                _likeService.UnlikePost(id, Context.UserIdentifier);
            }
            else
            {
                _likeService.LikePost(id, Context.UserIdentifier);
            }
            await Clients.All.SendAsync("Like", id, isLiked);
        }
    }
}
