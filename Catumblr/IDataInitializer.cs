﻿using System.Threading.Tasks;

namespace Catumblr
{
    public interface IDataInitializer
    {
        Task Initialize();
    }
}
