﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.DAL.Models;
using Catumblr.Models.ViewModels;

namespace Catumblr.Services
{
    public static class ViewModelService
    {
        public static PostsViewModel GetPostsViewModel(IEnumerable<Post> posts,
            string searchValue = null,
            bool searchInTitle = false,
            bool searchInContent = false,
            IEnumerable<string> tags = null,
            int skipCount = 0, int takeCount = 10
        )
        {
            return new PostsViewModel(posts, skipCount, takeCount)
            {
                Search =
                {
                    InTitle = searchInTitle,
                    InContent = searchInContent,
                    Value = searchValue,
                    Tags = tags
                }
            };
        }

        public static BlogPostsViewModel GetBlogPostsViewModel(IEnumerable<Post> posts, 
            int skipCount, int takeCount, int blogId, string blogTitle)
        {
            return new BlogPostsViewModel(posts, skipCount, takeCount)
            {
                BlogId = blogId,
                BlogTitle = blogTitle
            };
        }

        public static PostViewModel GetPostViewModel(Post post, string userId = null, bool hasRightEditOrDelete = false)
        {
            var isPostLiked = false;
            if (userId != null)
            {
               isPostLiked = post.Likes.Any(like => like.UserId == userId);
            }
            return new PostViewModel
            {
                Comments = post.Comments,
                Post = post,
                PostId = post.Id,
                IsLiked = isPostLiked,
                Title = post.Title,
                HasRightEditOrDelete = hasRightEditOrDelete
            };
        }

        public static StatisticViewModel GetStatisticViewModel(IEnumerable<Post> posts, int likeCount, int commentCount)
        {
            return new StatisticViewModel
            {
                Posts = posts,
                LikeCount = likeCount,
                CommentCount = commentCount
            };
        }
    }
}
