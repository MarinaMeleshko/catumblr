﻿using System.Threading.Tasks;
using Catumblr.BLL.DTO;
using Catumblr.BLL.ServiceInterfaces;
using Microsoft.AspNetCore.SignalR;

namespace Catumblr
{
    public class CommentHub : Hub
    {
        private readonly ICommentService _commentService;

        public CommentHub(ICommentService commentService)
        {
            _commentService = commentService;
        }

        public async Task Add(int postId, string text)
        {
            var comment = new CommentDto {PostId = postId, Text = text};
            _commentService.Add(comment, Context.User.Identity.Name, Context.UserIdentifier);
            await Clients.All.SendAsync("Add", postId, text, Context.User.Identity.Name);
        }
    }
}
