﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.Models.ViewModels
{
    public class PostsViewModel
    {
        public IEnumerable<Post> Posts { get; set; }
        public PaginationViewModel Pagination { get; set; }

        public SearchViewModel Search { get; set; } = new SearchViewModel();

        public PostsViewModel(IEnumerable<Post> posts, int skipCount, int takeCount)
        {
            var enumerable = posts as Post[] ?? posts.ToArray();
            var totalItemsCount = enumerable.Length;

            Posts = enumerable.Skip(skipCount).Take(takeCount);
            Pagination = new PaginationViewModel(totalItemsCount, skipCount, takeCount);
        }
    }
}
