﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.Models.ViewModels
{
    public class BlogPostsViewModel
    {
        public BlogPostsViewModel(IEnumerable<Post> posts, int skipCount, int takeCount)
        {
            var enumerable = posts as Post[] ?? posts.ToArray();
            var totalItemsCount = enumerable.Length;

            Posts = enumerable.Skip(skipCount).Take(takeCount);
            Pagination = new PaginationViewModel(totalItemsCount, skipCount, takeCount);
        }

        public IEnumerable<Post> Posts { get; set; }
        public int BlogId { get; set; }
        public string BlogTitle { get; set; }
        public PaginationViewModel Pagination { get; set; }
    }
}
