﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.Models.ViewModels
{
    public class PostViewModel
    {
        public int PostId { get; set; }
        public Post Post { get; set; }
        public string Title { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public bool IsLiked { get; set; }

        public int GetLikesCount => Post.Likes.Count;

        public int GetCommentsCount => Comments.Count();

        public bool HasTags => Tags.Any();

        public IEnumerable<Tag> Tags => Post.Tags;

        public bool HasRightEditOrDelete { get; set; }
    }
}
