﻿using System.Collections.Generic;

namespace Catumblr.Models.ViewModels
{
    public class SearchViewModel
    {
        public bool InTitle { get; set; }
        public bool InContent { get; set; }
        public string Value { get; set; }
        public IEnumerable<string> Tags { get; set; } = new List<string>();
    }
}
