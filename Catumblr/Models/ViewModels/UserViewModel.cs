﻿using Microsoft.AspNetCore.Identity;

namespace Catumblr.Models.ViewModels
{
    public class UserViewModel
    {
        public IdentityUser User { get; set; }
        public bool IsAdmin { get; set; }
    }
}
