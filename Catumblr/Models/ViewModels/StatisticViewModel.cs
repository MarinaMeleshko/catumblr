﻿using System.Collections.Generic;
using Catumblr.DAL.Models;

namespace Catumblr.Models.ViewModels
{
    public class StatisticViewModel
    {
        public IEnumerable<Post> Posts { get; set; }
        public int LikeCount { get; set; }
        public int CommentCount { get; set; }
    }
}
