﻿using System;

namespace Catumblr.Models.ViewModels
{
    public class PaginationViewModel
    {
        public bool HasNextPage => PageNumber < TotalPageCount;
        public bool HasPreviousPage => PageNumber > 1;
        public int PageNumber { get; set; }
        public int TotalPageCount { get; set; }
        public int PageSize { get; set; } = 3;

        public PaginationViewModel(int itemsCount = 0, int skipCount = 0, int takeCount = 3)
        {
            TotalPageCount = (int) Math.Ceiling((double) itemsCount / PageSize);
            PageNumber = skipCount / takeCount + 1;
        }
    }
}
