﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Catumblr
{
    public class PageItemCountConstraint: IRouteConstraint
    {
        public bool Match(HttpContext httpContext, 
            IRouter route, string routeKey, 
            RouteValueDictionary values, 
            RouteDirection routeDirection)
        {
            var stringCount = values[routeKey]?.ToString();
            var result = int.TryParse(stringCount, out var count);
            if (!result) return false;
            return (3 <= count) && (count <= 10);
        }
    }
}
