﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace Catumblr.Controllers
{
    public class BaseController : Controller
    {
        protected string UserId => User.Identity.IsAuthenticated ? User.FindFirst(ClaimTypes.NameIdentifier).Value : null;
        protected string UserName => User.Identity.Name;
    }
}