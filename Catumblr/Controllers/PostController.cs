﻿using System.Collections.Generic;
using Catumblr.BLL.DTO;
using Catumblr.BLL.ServiceInterfaces;
using Catumblr.Services;
using Microsoft.AspNetCore.Mvc;

namespace Catumblr.Controllers
{
    public class PostController : BaseController
    {
        private readonly IPostService _postService;
        private readonly ILikeService _likeService;
        private readonly IBlogService _blogService;

        public PostController(IPostService postService, 
            ILikeService likeService, 
            IBlogService blogService)
        {
            _postService = postService;
            _likeService = likeService;
            _blogService = blogService;
        }

        public IActionResult Index(int skipCount, int takeCount)
        {
            var posts = _postService.GetPosts(skipCount, takeCount);
            return View(posts);
        }

        public ActionResult BlogPosts(int blogId, string blogTitle, int skipCount = 0, int takeCount = 10)
        {
            var posts = _postService.GetBlogPosts(blogId);
            var blogPostsViewModel =
                ViewModelService.GetBlogPostsViewModel(posts, skipCount, takeCount, blogId, blogTitle);
            return View(blogPostsViewModel);
        }

        public ActionResult Create()
        {
            ViewBag.Blogs = _blogService.GetUserBlogs(UserId);
            return View();
        }

        [HttpPost]
        public ActionResult Create(PostDto postDto, ICollection<string> tags)
        {
            var post = _postService.Add(postDto, tags);
            var postViewModel = ViewModelService.GetPostViewModel(post);
            return View("Post", postViewModel);
        }

        public ActionResult Edit(int id)
        {
            var postDto = _postService.GetPostDtoById(id);
            return View(postDto);
        }

        [HttpPost]
        public ActionResult Edit(PostDto postDto, IEnumerable<string> tags)
        {
            _postService.Save(postDto, tags);
            return RedirectToAction("GetPost", new {id = postDto.Id});
        }

        public ActionResult GetPost(int id)
        {
            var post = _postService.GetPostById(id);
            var isAdmin = User.IsInRole("Admin");
            var isModerator = User.IsInRole("Moderator");
            var hasRightEditOrDelete = _postService.HasRightEditOrDelete(post, UserId, isAdmin, isModerator);
            var postViewModel = ViewModelService.GetPostViewModel(post, UserId, hasRightEditOrDelete);
            return View("Post", postViewModel);
        }

        public ActionResult Delete(int id)
        {
            _postService.Delete(id);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Like(int postId)
        {
            _likeService.LikePost(postId, UserId);
            return RedirectToAction("GetPost", new { id = postId });
        }

        public ActionResult Unlike(int postId)
        {
            _likeService.UnlikePost(postId, UserId);
            return RedirectToAction("GetPost", new { id = postId });
        }
    }
}