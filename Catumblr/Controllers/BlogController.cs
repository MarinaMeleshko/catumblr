﻿using Catumblr.BLL.DTO;
using Catumblr.BLL.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;

namespace Catumblr.Controllers
{
    public class BlogController : BaseController
    {
        private readonly IBlogService _blogService;

        public BlogController(IBlogService blogService)
        {
            _blogService = blogService;
        }

        public IActionResult Index()
        {
            var blogs = _blogService.GetUserBlogs(UserId);
            return View(blogs);
        }

        [HttpPost]
        public ActionResult Create(BlogDto blogDto)
        {
            _blogService.Add(blogDto, UserId);
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Delete(int blogId)
        {
            _blogService.Delete(blogId);
            return RedirectToAction("Index");
        }
    }
}