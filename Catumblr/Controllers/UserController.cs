﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catumblr.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Catumblr.Controllers
{
    public class UserController : BaseController
    {
        private readonly UserManager<IdentityUser> _userManager;

        public UserController(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            if (User.IsInRole("ADMIN"))
                return View(_userManager.Users.ToList());
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return RedirectToAction("Index");
            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains("Admin"))
            {
                ViewBag.Message = "You can't delete admin!";
                return RedirectToAction("Index");
            }
            await _userManager.DeleteAsync(user);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return RedirectToAction("Index");
            var isAdmin = (await _userManager.GetRolesAsync(user)).Contains("Admin");
            var userViewModel = new UserViewModel {User = user, IsAdmin = isAdmin};
            return View(userViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, bool isAdmin)
        {
            var user = await _userManager.FindByIdAsync(id);
            var isUserAdmin = (await _userManager.GetRolesAsync(user)).Contains("Admin");

            if (isUserAdmin && !isAdmin)
            {
                await _userManager.RemoveFromRoleAsync(user, "Admin");
            }

            if (!isUserAdmin && isAdmin)
            {
                await _userManager.AddToRoleAsync(user, "Admin");
            }

            return RedirectToAction("Edit", new {id});
        }
    }
}