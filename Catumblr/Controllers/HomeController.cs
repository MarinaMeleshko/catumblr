﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Catumblr.BLL.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Catumblr.Models;
using Catumblr.Models.ViewModels;
using Catumblr.Services;

namespace Catumblr.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISearchService _searchService;

        public HomeController(ISearchService searchService)
        {
            _searchService = searchService;
        }
        public IActionResult Index(string searchValue = null, 
            bool searchInTitle = false, 
            bool searchInContent = false, 
            IEnumerable<string> tags = null, 
            int skipCount = 0, int takeCount = 10)
        {
            var posts = _searchService.Search(searchValue, searchInTitle, searchInContent, tags);

            posts = posts
                .OrderBy(post => post.Time).Reverse()
                .OrderBy(post=>post.Likes.Count).Reverse();

            var postsViewModel = ViewModelService.GetPostsViewModel(posts, searchValue, searchInTitle, searchInContent,
                tags, skipCount, takeCount);

            return View(postsViewModel);
        }

        public IActionResult About()
        {
            ViewBag.Message = "Catumblr is cute bloghost";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
