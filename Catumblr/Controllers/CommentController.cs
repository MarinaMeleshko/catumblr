﻿using Catumblr.BLL.DTO;
using Catumblr.BLL.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;

namespace Catumblr.Controllers
{
    public class CommentController : BaseController
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        public ActionResult Add(int postId)
        {
            return View("_Add");
        }

        [HttpPost]
        public ActionResult Add(CommentDto commentDto)
        {           
            _commentService.Add(commentDto, UserName, UserId);
            return RedirectToAction("GetPost", "Post", new {id = commentDto.PostId});
        }
    }
}