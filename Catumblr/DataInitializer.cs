﻿using System.Linq;
using System.Threading.Tasks;
using Catumblr.DAL.Data;
using Catumblr.DAL.Models;
using Microsoft.AspNetCore.Identity;

namespace Catumblr
{
    public class DataInitializer : IDataInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public DataInitializer(ApplicationDbContext context, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }
        public async Task Initialize()
        {
            _context.Database.EnsureCreated();

            string[] roleNames = {"Admin"};
            foreach (var role in roleNames)
            {
                if (!await _roleManager.RoleExistsAsync(role))
                {
                    await _roleManager.CreateAsync(new IdentityRole(role));
                }
            }

            var userAdmin = await _userManager.FindByEmailAsync("irmelen909@gmail.com");
            var normalizedName = (await _roleManager.FindByNameAsync("Admin")).NormalizedName;
            var result = await _userManager.AddToRoleAsync(userAdmin, normalizedName);

            if (_context.Blogs.Any()) return;

            if (!_context.Users.Any())
            {
                var user = new IdentityUser { UserName = "test@gmail.com", Email = "test@gmail.com" };
                await _userManager.CreateAsync(user, "123-Password");
                _context.SaveChanges();
            }

            var firstUserId = _context.Users.First().Id;
            var firstUserName = _context.Users.First().UserName;

            var blog1 = new Blog {Title = "Blog", UserId = firstUserId};
            var blog2 = new Blog {Title = "Blog2", UserId = firstUserId};

            var post1 = new Post {Title = "Title", Content = "Content"};
            var post2 = new Post {Title = "Title2", Content = "Content"};

            blog1.Posts.Add(post1);
            blog1.Posts.Add(post2);

            var comment1 = new Comment {Text = "Text", UserId = firstUserId, UserName = firstUserName};
            var comment2 = new Comment {Text = "Text", UserId = firstUserId, UserName = firstUserName};

            post1.Comments.Add(comment1);
            post2.Comments.Add(comment2);

            _context.Blogs.AddRange(blog1, blog2);
            _context.SaveChanges();
        }
    }
}
