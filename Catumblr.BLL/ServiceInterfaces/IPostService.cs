﻿using System.Collections.Generic;
using Catumblr.BLL.DTO;
using Catumblr.DAL.Models;

namespace Catumblr.BLL.ServiceInterfaces
{
    public interface IPostService
    {
        IEnumerable<Post> GetPosts(int skipCount, int takeCount);
        Post GetPostById(int postId);
        PostDto GetPostDtoById(int postId);
        IEnumerable<Post> GetBlogPosts(int blogId);
        Post Add(PostDto postDto, IEnumerable<string> tags);
        void Save(PostDto postDto, IEnumerable<string> tags);
        void Delete(int postId);
        IEnumerable<Post> GetAllPosts();
        IEnumerable<Post> GetUserPosts(string userId);
        IEnumerable<Post> GetMostPopularPosts(string userId, int count);
        bool HasRightEditOrDelete(Post post, string userId, bool isAdmin, bool isModerator);
    }
}
