﻿using System.Collections.Generic;
using Catumblr.BLL.DTO;
using Catumblr.DAL.Models;

namespace Catumblr.BLL.ServiceInterfaces
{
    public interface IBlogService
    {
        IEnumerable<Blog> GetUserBlogs(string userId);
        void Add(BlogDto blogDto, string userId);
        void Delete(int blogId);
    }
}
