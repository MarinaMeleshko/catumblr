﻿namespace Catumblr.BLL.ServiceInterfaces
{
    public interface ILikeService
    {
        void LikePost(int postId, string userId);
        void UnlikePost(int postId, string userId);
    }
}
