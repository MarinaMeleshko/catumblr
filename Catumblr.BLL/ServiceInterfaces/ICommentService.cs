﻿using Catumblr.BLL.DTO;

namespace Catumblr.BLL.ServiceInterfaces
{
    public interface ICommentService
    {
        void Add(CommentDto commentDto, string userName, string userId);
    }
}
