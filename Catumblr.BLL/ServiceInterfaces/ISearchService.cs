﻿using System.Collections.Generic;
using Catumblr.DAL.Models;

namespace Catumblr.BLL.ServiceInterfaces
{
    public interface ISearchService
    {
        IEnumerable<Post> Search(string searchValue,
            bool searchInTitle,
            bool searchInContent,
            IEnumerable<string> tags);
    }
}
