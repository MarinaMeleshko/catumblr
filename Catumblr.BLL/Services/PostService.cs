﻿using System;
using System.Collections.Generic;
using System.Linq;
using Catumblr.BLL.DTO;
using Catumblr.BLL.ServiceInterfaces;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.BLL.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _repository;

        public PostService(IPostRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return _repository.GetAllPosts();
        }

        public IEnumerable<Post> GetPosts(int skipCount, int takeCount)
        {
            return _repository.GetAllPosts().Skip(skipCount).Take(takeCount);
        }

        public IEnumerable<Post> GetUserPosts(string userId)
        {
            return _repository.GetAllPosts().Where(post => post.Blog.UserId == userId);
        }


        public IEnumerable<Post> GetMostPopularPosts(string userId, int count)
        {
            return GetUserPosts(userId)
                .OrderBy(post => post.Likes.Count + post.Comments.Count).Reverse().Take(count);
        }

        public Post GetPostById(int postId)
        {
            return _repository.GetById(postId);
        }

        public PostDto GetPostDtoById(int postId)
        {
            var post = GetPostById(postId);
            return new PostDto
            {
                Id = post.Id,
                Title = post.Title,
                Content = post.Content,
                Comments = post.Comments,
                Likes = post.Likes,
                Tags = post.Tags
            };
        }

        public IEnumerable<Post> GetBlogPosts(int blogId)
        {
            return _repository.GetAllPosts().Where(post => post.BlogId == blogId);
        }

        public Post Add(PostDto postDto, IEnumerable<string> tags)
        {
            var post = new Post
            {
                Title = postDto.Title,
                Content = postDto.Content,
                BlogId = postDto.BlogId,
                Time = DateTime.Now
            };
            var tagsCollection = tags.Select(tag => new Tag { Name = tag }).ToList();
            _repository.AddPost(post, tagsCollection);
            return post;
        }

        public void Save(PostDto postDto, IEnumerable<string> tags)
        {
            var tagsCollection = tags.Select(tag => new Tag {Name = tag}).ToList();
            var post = GetPostById(postDto.Id);
            post.Title = postDto.Title;
            post.Content = postDto.Content;

            _repository.SavePost(post, tagsCollection);
        }

        public void Delete(int postId)
        {
            _repository.DeletePost(postId);
        }

        public bool HasRightEditOrDelete(Post post, string userId, bool isAdmin, bool isModerator)
        {
            return post.Blog.UserId == userId || isAdmin || isModerator;
        }
    }
}
