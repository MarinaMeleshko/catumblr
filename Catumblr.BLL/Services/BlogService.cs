﻿using System;
using System.Collections.Generic;
using System.Linq;
using Catumblr.BLL.DTO;
using Catumblr.BLL.ServiceInterfaces;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.BLL.Services
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _repository;

        public BlogService(IBlogRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Blog> GetUserBlogs(string userId)
        {
            return _repository.GetAllBlogs().Where(blog => blog.UserId == userId);
        }

        public void Add(BlogDto blogDto, string userId)
        {
            var blog = new Blog
            {
                UserId = userId,
                Date = DateTime.Today,
                Title = blogDto.Title
            };
            _repository.AddBlog(blog);
        }

        public void Delete(int blogId)
        {
            _repository.DeleteBlog(blogId);
        }
    }
}
