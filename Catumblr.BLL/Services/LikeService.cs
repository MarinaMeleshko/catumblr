﻿using System.Linq;
using Catumblr.BLL.ServiceInterfaces;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.BLL.Services
{
    public class LikeService : ILikeService
    {
        private readonly IPostRepository _repository;

        public LikeService(IPostRepository repository)
        {
            _repository = repository;
        }
        public void LikePost(int postId, string userId)
        {
            var post = _repository.GetById(postId);
            if (post == null) return;
            if (post.Likes.Any(like => like.UserId == userId)) return;
            _repository.LikePost(post, userId);
        }

        public void UnlikePost(int postId, string userId)
        {
            var post = _repository.GetById(postId);
            if (post == null) return;
            _repository.UnlikePost(post, userId);
        }
    }
}
