﻿using System;
using Catumblr.BLL.DTO;
using Catumblr.BLL.ServiceInterfaces;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _repository;

        public CommentService(ICommentRepository repository)
        {
            _repository = repository;
        }

        public void Add(CommentDto commentDto, string userName, string userId)
        {
            var comment = new Comment
            {
                UserName = userName,
                UserId = userId,
                PostId = commentDto.PostId,
                Time = DateTime.Now,
                Text = commentDto.Text
            };
            _repository.AddComment(comment);
        }
    }
}
