﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.BLL.ServiceInterfaces;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.BLL.Services
{
    public class SearchService : ISearchService
    {
        private delegate bool Predicate(Post post);

        private readonly IPostRepository _postRepository;

        public SearchService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        private static Predicate ConfigureSearchPredicate(string searchValue, bool searchInTitle, bool searchInContent)
        {
            Predicate predicate = post => true;
            if (searchInTitle)
            {
                predicate = post => post.Title == null || post.Title.ToLower().Contains(searchValue.ToLower());
            }

            if (!searchInContent) return predicate;
            
            var oldPredicate = predicate;
            predicate = post => post.Content == null || oldPredicate(post) || post.Content.ToLower().Contains(searchValue.ToLower());

            return predicate;
        }

        private IEnumerable<Post> SearchByString(string searchValue, bool searchInTitle, bool searchInContent)
        {
            var posts = _postRepository.GetAllPosts();
            if (string.IsNullOrEmpty(searchValue)) return posts;

            var predicate = ConfigureSearchPredicate(searchValue, searchInTitle, searchInContent);

            posts = posts.Where(post => predicate(post));
            return posts;
        }

        private IEnumerable<Post> SearchByTags(IEnumerable<string> tags)
        {         
            if (tags == null || !tags.Any()) return new List<Post>();

            var posts = _postRepository.GetAllPosts();

            posts = posts.Where(post => post.Tags.Any(tag => tags.Contains(tag.Name)));
            return posts;
        }

        public IEnumerable<Post> Search(string searchValue,
            bool searchInTitle,
            bool searchInContent,
            IEnumerable<string> tags)
        {
            var searchValueResult = SearchByString(searchValue, searchInTitle, searchInContent);
            var tagsResult = SearchByTags(tags);
            return searchValueResult.Union(tagsResult);
        }
    }
}
