﻿namespace Catumblr.BLL.DTO
{
    public class CommentDto
    {
        public string Text { get; set; }
        public int PostId { get; set; }
    }
}
