﻿using System.Collections.Generic;
using Catumblr.DAL.Models;

namespace Catumblr.BLL.DTO
{
    public class PostDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public IEnumerable<Tag> Tags { get; set; } = new List<Tag>();
        public IEnumerable<Like> Likes { get; set; } = new List<Like>();
        public IEnumerable<Comment> Comments { get; set; } = new List<Comment>();
        public int BlogId { get; set; }
    }
}
