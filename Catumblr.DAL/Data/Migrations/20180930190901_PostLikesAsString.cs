﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Catumblr.DAL.Data.Migrations
{
    public partial class PostLikesAsString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LikesAsString",
                table: "Posts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LikesAsString",
                table: "Posts");
        }
    }
}
