﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Catumblr.DAL.Models
{
    public class Blog
    {
        public Blog()
        {
            Posts = new HashSet<Post>();
        }

        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public ICollection<Post> Posts { get; }

        public string UserId { get; set; }

        public DateTime Date { get; set; }
    }
}
