﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Catumblr.DAL.Models
{
    public class Post
    {
        public Post()
        {
            Comments = new HashSet<Comment>();
            Likes = new HashSet<Like>();
        }

        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public ICollection<Comment> Comments { get; }

        public int BlogId { get; set; }

        public Blog Blog { get; set; }

        public ICollection<Like> Likes { get; }

        public DateTime Time { get; set; }

        public virtual ICollection<PostTag> PostTags { get; set; } = new List<PostTag>();

        [NotMapped]
        public IEnumerable<Tag> Tags => PostTags.Select(pt => pt.Tag).ToList();
    }
}
