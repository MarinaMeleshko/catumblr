﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Catumblr.DAL.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime Time { get; set; }
    }
}
