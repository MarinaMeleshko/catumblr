﻿using System.Collections.Generic;

namespace Catumblr.DAL.Models
{
    public class Role
    {
        public Role()
        {
            Users = new HashSet<string>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<string> Users { get; }
    }
}
