﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Catumblr.DAL.Models
{
    public class Tag
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [NotMapped]
        public IEnumerable<Post> Posts => PostTags.Select(pt => pt.Post);

        public virtual ICollection<PostTag> PostTags { get; set; } = new List<PostTag>();
    }
}
