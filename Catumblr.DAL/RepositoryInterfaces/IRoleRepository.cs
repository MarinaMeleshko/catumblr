﻿using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.DAL.RepositoryInterfaces
{
    public interface IRoleRepository
    {
        IQueryable<Role> GetAllRoles();
        Role GetByName(string name);
        void AddRole(string name);
        void DeleteRole(string name);
    }
}
