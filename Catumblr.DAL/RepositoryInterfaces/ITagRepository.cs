﻿using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.DAL.RepositoryInterfaces
{
    public interface ITagRepository
    {
        IQueryable<Tag> GetAllTags();
        Tag GetOrCreate(string name);
        Tag AddTag(string name);
    }
}
