﻿using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.DAL.RepositoryInterfaces
{
    public interface IBlogRepository
    {
        IQueryable<Blog> GetAllBlogs();
        void AddBlog(Blog blog);
        void DeleteBlog(int id);
    }
}
