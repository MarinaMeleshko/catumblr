﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.DAL.RepositoryInterfaces
{
    public interface IPostRepository
    {
        IQueryable<Post> GetAllPosts();
        Post GetById(int id);
        void AddPost(Post post, ICollection<Tag> tags);
        void SavePost(Post post, ICollection<Tag> tags);
        void DeletePost(int id);
        void LikePost(Post post, string userId);
        void UnlikePost(Post post, string userId);
    }
}
