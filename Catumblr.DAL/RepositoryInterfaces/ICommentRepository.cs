﻿using System.Linq;
using Catumblr.DAL.Models;

namespace Catumblr.DAL.RepositoryInterfaces
{
    public interface ICommentRepository
    {
        IQueryable<Comment> GetAllComments();
        Comment GetById(int id);
        void AddComment(Comment comment);
        void SaveComment(Comment comment);
        void DeleteComment(int id);
    }
}
