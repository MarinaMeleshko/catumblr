﻿using System.Linq;
using Catumblr.DAL.Data;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.DAL.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly ApplicationDbContext _context;

        public CommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IQueryable<Comment> GetAllComments()
        {
            return _context.Comments;
        }

        public Comment GetById(int id)
        {
            return _context.Comments.FirstOrDefault(comment => comment.Id == id);
        }

        public void AddComment(Comment comment)
        {
            _context.Comments.Add(comment);
            _context.SaveChanges();
        }

        public void SaveComment(Comment comment)
        {
            _context.Comments.Update(comment);
            _context.SaveChanges();
        }

        public void DeleteComment(int id)
        {
            var comment = GetById(id);
            if (comment == null) return;
            _context.Comments.Remove(comment);
            _context.SaveChanges();
        }
    }
}
