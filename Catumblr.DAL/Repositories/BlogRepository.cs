﻿using System.Linq;
using Catumblr.DAL.Data;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.DAL.Repositories
{
    public class BlogRepository : IBlogRepository
    {
        private readonly ApplicationDbContext _context;

        public BlogRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IQueryable<Blog> GetAllBlogs()
        {
            return _context.Blogs;
        }

        public void AddBlog(Blog blog)
        {
            _context.Blogs.Add(blog);
            _context.SaveChanges();
        }

        public void DeleteBlog(int id)
        {
            var blog = _context.Blogs.FirstOrDefault(b => b.Id == id);
            if (blog == null) return;
            _context.Blogs.Remove(blog);
            _context.SaveChanges();
        }
    }
}
