﻿using System.Linq;
using Catumblr.DAL.Data;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.DAL.Repositories
{
    public class TagRepository : ITagRepository
    {
        private readonly ApplicationDbContext _context;

        public TagRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IQueryable<Tag> GetAllTags()
        {
            return _context.Tags;
        }

        public Tag AddTag(string name)
        {
            var tag = new Tag {Name = name};
            _context.Tags.Add(tag);
            _context.SaveChanges();
            return tag;
        }

        public Tag GetOrCreate(string name)
        {
            var tag = GetAllTags().FirstOrDefault(t => t.Name == name);
            return tag ?? AddTag(name);
        }
    }
}
