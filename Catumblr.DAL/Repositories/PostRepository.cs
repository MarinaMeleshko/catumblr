﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.DAL.Data;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;

namespace Catumblr.DAL.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly ApplicationDbContext _context;

        public PostRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Post> GetAllPosts()
        {
            return _context.Posts
                .Include(post => post.PostTags)
                .ThenInclude(postTag => postTag.Tag)
                .Include(post => post.Comments)
                .Include(post => post.Likes)
                .Include(post => post.Blog);
        }

        public Post GetById(int id)
        {
            return GetAllPosts().FirstOrDefault(post => post.Id == id);
        }

        public void AddPost(Post post, ICollection<Tag> tags)
        {
            var tagCollection = tags.Select(AddTag).ToList();

            var postTagCollection = tagCollection
                .Select(tag => new PostTag {Post = post, Tag = tag}).ToList();

            post.PostTags = postTagCollection;
            _context.AddRange(postTagCollection);
            _context.SaveChanges();
        }
        
        public void SavePost(Post post, ICollection<Tag> tags)
        {
            var tagCollection = tags.Select(AddTag).ToList();
            var postTagCollection = tagCollection
                .Select(tag => new PostTag { Post = post, Tag = tag}).ToList();

            _context.RemoveRange(post.PostTags);
            post.PostTags = postTagCollection;
            
            _context.Posts.Update(post);
            _context.SaveChanges();
        }

        public void DeletePost(int id)
        {
            var post = _context.Posts.FirstOrDefault(p => p.Id == id);
            if (post == null) return;
            _context.Posts.Remove(post);
            _context.SaveChanges();
        }

        private IQueryable<Tag> GetAllTags()
        {
            return _context.Tags;
        }

        private Tag AddTag(Tag tag)
        {
            var tagByName = GetAllTags().FirstOrDefault(t => t.Name == tag.Name);
            if (tagByName != null) return tagByName;
            _context.Tags.Add(tag);
            _context.SaveChanges();
            return tag;
        }

        public void LikePost(Post post, string userId)
        {
            post.Likes.Add(new Like {Post = post, UserId = userId});
            _context.Posts.Update(post);
            _context.SaveChanges();
        }

        public void UnlikePost(Post post, string userId)
        {
            var like = post?.Likes.FirstOrDefault(l => l.UserId == userId);
            if (like == null) return;
            post.Likes.Remove(like);
            _context.Posts.Update(post);
            _context.SaveChanges();
        }
    }
}
