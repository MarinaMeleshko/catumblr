﻿using System.Collections.Generic;
using System.Linq;
using Catumblr.DAL.Models;
using Catumblr.DAL.RepositoryInterfaces;

namespace Catumblr.DAL.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        public IQueryable<Role> GetAllRoles()
        {
            return new List<Role>().AsQueryable();
        }

        public Role GetByName(string name)
        {
            return GetAllRoles().FirstOrDefault(role => role.Name == name);
        }

        public void AddRole(string name)
        { }

        public void DeleteRole(string name)
        { }
    }
}
